
## Description

elevators dispatcher intreview question

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Routes
GET http://localhost:3000/go/from/to
```
GET http://localhost:3000/go/1/10
```
