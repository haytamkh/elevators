export default () => ({
    totalElevators: parseInt(process.env.totalElevators) || 2,
    totalFloors: parseInt(process.env.totalElevators) || 10,
  });