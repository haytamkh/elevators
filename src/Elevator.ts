import * as _ from 'lodash';
export enum ElevatorStatus {
    Idle = 0,
    Moving = 1,
}

export enum ElevatorDirection {
    Up = 0,
    Down = 1,
}
export interface IElevator {
    getFloorScore(fromFloor: number, direction: ElevatorDirection): number;
    planStop(from: number, to: number): void;
    getEstimatedWaitingTime(from: number): number
}
export class Elevator implements IElevator {
    private status: ElevatorStatus;
    private direction: ElevatorDirection;
    private position: number;
    private plan; // Map for scheduled plans
    private timer;
    private timePerFloorInMS = 2000;
    private stops; // Set of floors to stop per direction
    constructor(private logger, private totalFloors: number, public name: string = 'elevator1', private minimumFloor = 1) {
        this.status = ElevatorStatus.Idle;
        this.direction = ElevatorDirection.Up;
        this.plan = new Map();
        this.stops = new Set();
        this.position = minimumFloor;
    }

    public getFloorScore(fromFloor: number, direction: ElevatorDirection): number {
        const plusScore = +(direction === this.direction);
        return (this.totalFloors + plusScore) - Math.abs(this.position - fromFloor);
    }
    public planStop(from: number, to: number): void {
        this.plan.set(Number(from), Number(to));
        this.stops.add(Number(from));
        this.next();
    }

    public getEstimatedWaitingTime(from: number): number {
        return Math.abs(this.position - from) * this.timePerFloorInMS / 1000;
    }
    private shouldStop() {
        return this.stops.has(this.position) || !this.stops.size;
    }

    private switchDirection() {
        this.direction = this.direction ? ElevatorDirection.Up : ElevatorDirection.Down;
    }

    private shouldSwitchDirection() {
        let shouldSwitch = false;
        if (this.direction === ElevatorDirection.Up) {
            shouldSwitch = _.find(Array.from(this.stops), (o) => o >= this.position);
        } else {
            shouldSwitch = _.find(Array.from(this.stops), (o) => o <= this.position);
        }
        return !shouldSwitch;
    }


    private pickPassenger() {
        this.logger.warn(`${this.name} stopping in floor ${this.position}`);
        this.stops.delete(this.position);
        clearInterval(this.timer);
        this.status = ElevatorStatus.Idle;
        const next = this.plan.get(this.position);
        this.plan.delete(this.position);

        if (next) {
            this.stops.add(next);
        }
    }
    private updateState() {
        if (this.direction === ElevatorDirection.Up) {
            this.position++;
        } else {
            this.position--;
        }
        this.logger.log(`${this.name} in floor ${this.position}`);
        if (this.shouldStop()) {
            this.pickPassenger();
            this.next();
        }
    }

    private next() {
        if (this.status != ElevatorStatus.Idle) {
            return;
        }
        if (!this.stops.size) {
            return;
        }

        // check if the next floor is in the same direction , otherwise switch directon and move
        if (this.shouldSwitchDirection()) {
            this.logger.log(`${this.name} switching direction`);
            this.switchDirection();
        }

        // if need to pick someome from current floor
        if (this.shouldStop()) {
            this.pickPassenger();
            this.next();
            return;
        }


        this.status = ElevatorStatus.Moving;
        this.timer = setInterval(() => {
            this.updateState();
        }, this.timePerFloorInMS);

    }

} 