import { Controller, Get , Param} from '@nestjs/common';
import { AppService } from './app.service';
import { MissionPlanner } from './MissionPlanner';

@Controller()
export class AppController {
  constructor(private readonly missionPlanner: MissionPlanner) {
  }

  @Get()
  getHello(): string {
    return 'service is running';
  }


  @Get('go/:from/:to')
  goToFloor(@Param() params): object {
    return this.missionPlanner.goToFloor(params.from,params.to);
  }


}
