import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Elevator, ElevatorDirection, IElevator } from './Elevator';

@Injectable()
export class MissionPlanner implements OnModuleInit {
    private totalFloors: number;
    protected elevators: IElevator[] = [];
    constructor(private config: ConfigService, private logger: Logger) { }

    async onModuleInit(): Promise<void> {
        await this.init();
    }

    async init(): Promise<void> {
        this.totalFloors = this.config.get('totalFloors');
        for (let i = 1; i <= this.config.get('totalElevators'); i++) {
            this.logger.log(`initilizing elevator ${i}`);
            this.elevators.push(new Elevator(this.logger, this.totalFloors, `elevator_${i}`));
        }
    }

    private selectBestElevator(passengerFloor: number, direction: ElevatorDirection) {
        let maxScore = 0;
        let selectedElevator = null;
        this.elevators.forEach(elevator => {
            const score = elevator.getFloorScore(passengerFloor, ElevatorDirection.Up);
            if (score > maxScore) {
                maxScore = score;
                selectedElevator = elevator;
            }
        });
        return selectedElevator;


    }
    private getDirection(from: number, to: number): ElevatorDirection {
        return from > to ? ElevatorDirection.Down : ElevatorDirection.Up;
    }

    async goToFloor(from: number, to: number) {
        from = Number(from);
        to = Number(to);
        if (from > this.totalFloors || to > this.totalFloors) {
            throw new Error('from and to params should be in range of totalFloors');
        }

        const direction = this.getDirection(from, to);
        const bestElevator = this.selectBestElevator(from, direction);


        bestElevator.planStop(from, to);
        const estimatedTime = bestElevator.getEstimatedWaitingTime(from);
        return { elevator: bestElevator.name, estimatedTime: `${estimatedTime} seconds` };

    }
}
