import { Logger, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import configuration from './config/config';
import { MissionPlanner } from './MissionPlanner';
@Module({
  imports: [ConfigModule.forRoot({ load: [configuration] })],
  controllers: [AppController],
  providers: [MissionPlanner, Logger],
})
export class AppModule { }
